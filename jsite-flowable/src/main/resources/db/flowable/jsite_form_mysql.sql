SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS flow_form_data;
DROP TABLE IF EXISTS flow_form;
DROP TABLE IF EXISTS flow_form_authorize;
DROP TABLE IF EXISTS flow_form_business;
DROP TABLE IF EXISTS flow_form_user;
DROP TABLE IF EXISTS flow_form_vars;
DROP TABLE IF EXISTS flow_form_version;




/* Create Tables */

-- 流程表单表
CREATE TABLE flow_form
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 流程模型Id
	model_id varchar(255) COMMENT '流程模型Id',
	-- 流程模型key
	model_key varchar(400) COMMENT '流程模型key',
	-- 流程模型版本
	model_version int COMMENT '流程模型版本',
	-- 表单内容
	form_content text COMMENT '表单内容',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记',
	PRIMARY KEY (id)
) COMMENT = '流程表单表';


-- 流程表单字段权限表
CREATE TABLE flow_form_authorize
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 流程模型Id
	model_id varchar(255) COMMENT '流程模型Id',
	-- 流程模型key
	model_key varchar(400) COMMENT '流程模型key',
	-- 流程模型版本
	model_version int COMMENT '流程模型版本',
	-- 字段名称
	field_name varchar(64) COMMENT '字段名称',
	-- 流程节点ID
	resource_id varchar(64) COMMENT '流程节点ID',
	-- 流程节点名称
	resource_name varchar(64) COMMENT '流程节点名称',
	-- 是否只读
	is_read char(1) COMMENT '是否只读',
	-- 是否隐藏
	is_hide char(1) COMMENT '是否隐藏',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记',
	PRIMARY KEY (id)
) COMMENT = '流程表单字段权限表';


-- 流程表单业务总表
CREATE TABLE flow_form_business
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 表单编号
	form_id varchar(64) COMMENT '表单编号',
	-- 流程实例ID
	proc_ins_id varchar(255) COMMENT '流程实例ID',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记',
	PRIMARY KEY (id)
) COMMENT = '流程表单业务总表';


-- 流程表单字段数据表
CREATE TABLE flow_form_data
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 表单编号
	form_id varchar(64) NOT NULL COMMENT '表单编号',
	-- 流程实例ID
	proc_ins_id varchar(255) COMMENT '流程实例ID',
	-- 字段名称
	field_name varchar(64) COMMENT '字段名称',
	-- 字段值
	field_value varchar(255) COMMENT '字段值',
	-- 大文本字段值
	field_large_value varchar(3000) COMMENT '大文本字段值',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记',
	PRIMARY KEY (id)
) COMMENT = '流程表单字段数据表';


-- 表单人员权限表
CREATE TABLE flow_form_user
(
	-- 模型key
	model_key varchar(64) COMMENT '模型key',
	-- 人员ID
	user_id varchar(64) COMMENT '人员ID'
) COMMENT = '表单人员权限表';


-- 流程表单变量表
CREATE TABLE flow_form_vars
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 流程模型Id
	model_id varchar(255) COMMENT '流程模型Id',
	-- 流程模型key
	model_key varchar(400) COMMENT '流程模型key',
	-- 流程模型版本
	model_version int COMMENT '流程模型版本',
	-- 字段名称
	field_name varchar(64) COMMENT '字段名称',
	-- 变量名称
	var_name varchar(255) COMMENT '变量名称',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记',
	PRIMARY KEY (id)
) COMMENT = '流程表单变量表';


-- 流程表单版本表
CREATE TABLE flow_form_version
(
	-- 流程实例ID
	proc_ins_id varchar(255) COMMENT '流程实例ID',
	-- 流程模型版本
	model_version int COMMENT '流程模型版本'
) COMMENT = '流程表单版本表';



/* Create Foreign Keys */

ALTER TABLE flow_form_data
	ADD FOREIGN KEY (form_id)
	REFERENCES flow_form (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



